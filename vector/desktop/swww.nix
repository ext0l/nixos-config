{ config, pkgs, lib, ... }:

with lib;

{
  home.packages = [ pkgs.swww ];

  systemd.user.services.swww = {
    Unit = {
      Description = "Anime girl displayer";
      Documentation = "https://github.com/LGFae/swww";
      PartOf = [ "graphical-session.target" ];
      After = [ "graphical-session-pre.target" ];
    };

    Service = {
      ExecStart = "${pkgs.swww}/bin/swww-daemon";
      Restart = "on-failure";
      KillMode = "mixed";
    };

    Install = { WantedBy = [ "graphical-session.target" ]; };
  };
}
