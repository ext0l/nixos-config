{ config, pkgs, lib, ... }:

with lib;

let
  inherit (lib.lists) imap1;
  inherit (lib.trivial) pipe;
  inherit (builtins) concatLists listToAttrs;
  cfg = config.wayland.windowManager.sway;
in {
  imports = [ ./swww.nix ];

  home.packages = with pkgs; [
    wofi
    sway-contrib.grimshot
    swaylock
    swayidle
    wev
    wl-clipboard
  ];

  home.file.".config/wofi/style.css" = { source = ./interstellar-wofi.css; };

  wayland.windowManager.sway = {
    enable = true;
    package = pkgs.swayfx;
    wrapperFeatures.gtk = true;

    checkConfig = false;

    extraConfig = ''
      for_window [app_id="scratchpad"] move to scratchpad
      exec systemctl --user import-environment
      title_align center
      titlebar_border_thickness 0
      exec_always pkill --signal SIGHUP kanshi
    '';

    config = {
      modifier = "Mod4";
      menu = "wofi --insensitive --show drun";
      terminal = "${pkgs.kitty}/bin/kitty";
      input = {
        "type:keyboard" = {
          xkb_layout = "us,us";
          xkb_variant = "colemak_dh_ortho,";
          xkb_options = "grp:rctrl_rshift_toggle,ctrl:nocaps";
          repeat_delay = "300";
          repeat_rate = "50";
        };
        "*" = {
          dwt = "disabled";
          tap_button_map = "lrm";
          click_method = "clickfinger";
        };
        # aleph's trackpad is pretty twitchy
        "2362:628:PIXA3854:00_093A:0274_Touchpad" = {
          scroll_factor = "0.6";
        };
        "1133:49738:Logitech_Gaming_Mouse_G600" = { pointer_accel = "-0.8"; };
        "1133:45091:Logitech_Wireless_Mouse_MX_Master_3" = {
          pointer_accel = "-0.5";
        };
      };

      gaps.outer = 6;
      window = {
        titlebar = true;
        border = 1;
      };

      colors = {
        focused = {
          border = "#00334f";
          background = "#00334f";
          text = "#ffffff";
          indicator = "#2e9ef4";
          childBorder = cfg.config.colors.unfocused.childBorder;
        };
        focusedInactive = {
          border = "#001e2e";
          background = "#001e2e";
          text = "#aaaaaa";
          indicator = "#484e50";
          childBorder = cfg.config.colors.unfocused.childBorder;
        };
        unfocused = {
          border = "#222222";
          background = "#222222";
          text = "#888888";
          indicator = "#292d2e";
          childBorder = "#222222";
        };
      };

      seat = {
        seat0 = {
          # not sure why sway doesn't pick up the size from XCURSOR_SIZE
          xcursor_theme = "'Capitaine Cursors' 36";
        };
      };

      # We manage waybar via systemd separately.
      bars = [ ];

      keybindings = let
        modifier = "Mod4";
        workspaces = [ "GEN" "SNS" "DEV" "REF" "GBF" "MP3" "MSG" "-8-" "-9-" ];
        workspaceBindings = pipe workspaces [
          (imap1 (i: ws: [
            {
              name = "${modifier}+${toString i}";
              value = "workspace number ${toString i}:${ws}";
            }
            {
              name = "${modifier}+Shift+${toString i}";
              value = "move container to workspace ${toString i}:${ws}";
            }
          ]))
          concatLists
          listToAttrs
        ];
      in mapAttrs' (k: nameValuePair "${modifier}+${k}") {
        "Return" = "exec ${cfg.config.terminal}";
        "Shift+q" = "kill";
        "d" = "exec ${cfg.config.menu}";

        "m" = "focus left";
        "n" = "focus down";
        "e" = "focus up";
        "i" = "focus right";

        "Shift+m" = "move left";
        "Shift+n" = "move down";
        "Shift+e" = "move up";
        "Shift+i" = "move right";

        "a" = "focus parent";
        "z" = "focus child";
        "f" = "fullscreen toggle";

        "Shift+s" = "layout stacking";
        "Shift+t" = "layout tabbed";
        "Shift+v" = "layout splitv";
        "Shift+h" = "layout splith";

        "h" = "splith";
        "v" = "splitv";

        "Shift+r" = "reload";

        "r" = "mode resize";

        "Shift+space" = "floating toggle";
        "space" = "focus mode_toggle";

        "Minus" = "exec ${./scratchpad.sh}";
        "Backslash" = "sticky toggle";
      } // {
        "Print" = "exec grimshot copy screen";
        "Shift+Print" = "exec grimshot copy area";
        "Ctrl+Print" = "exec grimshot copy window";
      } // workspaceBindings // mapAttrs' (k: nameValuePair "--locked ${k}") {
        XF86AudioRaiseVolume = "exec pactl set-sink-volume @DEFAULT_SINK@ +10%";
        XF86AudioLowerVolume = "exec pactl set-sink-volume @DEFAULT_SINK@ -10%";
        XF86AudioMute = "exec pactl set-sink-mute @DEFAULT_SINK@ toggle";
        XF86AudioPlay = "exec cmus-remote --pause";
        XF86AudioStop = "exec cmus-remote --stop";
        XF86AudioPrev = "exec cmus-remote --prev";
        XF86AudioNext = "exec cmus-remote --next";
        XF86MonBrightnessUp = "exec brightnessctl set +5%";
        XF86MonBrightnessDown = "exec brightnessctl set 5%-";
        "Ctrl+XF86MonBrightnessUp" = "exec cosmosctl brightness --adjust 0.1";
        "Ctrl+XF86MonBrightnessDown" =
          "exec cosmosctl brightness --adjust -0.1";
        "Ctrl+Shift+XF86MonBrightnessDown" = "exec cosmosctl mode --reset";
        "Ctrl+Shift+XF86MonBrightnessUp" = "exec cosmosctl mode --reset";
      };
    };
  };

  systemd.user.services.locker = let target = "sway-session.target";
  in {
    Unit = {
      Description = "Automatid screen locker";
      Documentation = "man:swayidle(1)";
      BindsTo = [ target ];
    };
    Service = {
      Type = "simple";
      ExecStart = let
        lockCommand =
          "${pkgs.swaylock}/bin/swaylock -f --image ~/Documents/wallpapers/anime/saber-alter-motorcycle.jpeg";
        # sleep if we're not plugged in
        maybeSleepCommand =
          "[[ $(cat /sys/class/power_supply/AC/online) -eq 0 ]] && systemctl suspend";
      in ''
        ${pkgs.swayidle}/bin/swayidle -w \
          timeout 300 '${lockCommand}' \
          timeout 600 '${maybeSleepCommand}' \
          before-sleep '${lockCommand}' \
          timeout 360 'brightnessctl --save; swaymsg "output * dpms off"' \
          resume 'swaymsg "output * dpms on"; brightnessctl --restore' \
          lock '${lockCommand}' '';
      Restart = "always";
    };
    Install = { WantedBy = [ target ]; };
  };
}
