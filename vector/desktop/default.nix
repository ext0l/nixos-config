# All configuration that should only be applied on machines where I
# have a GUI. Right now, this is just superluminal-steel. Note that
# this should be imported via machines/common/desktop.nix, which
# imports this *and* the system-level configuration (fonts, etc). If I
# ever get a second 'desktop' this might wind up splitting.

{ config, pkgs, lib, ... }:

let
  vital = with pkgs;
    stdenv.mkDerivation rec {
      pname = "vital";
      version = "1.5.5";
      src = requireFile {
        message = "run nix-store add-file VitalInstaller.zip";
        name = "VitalInstaller.zip";
        # nix-hash --flat --type sha256 VitalInstaller.zip
        sha256 =
          "68f3c7e845f3d7a5b44a83adeb6e34ef221503df00e7964f7d5a1f132a252d13";
      };
      nativeBuildInputs = [ makeWrapper unzip ];
      buildInputs = [
        alsa-lib
        freetype
        libglvnd
        stdenv.cc.cc.lib
        xorg.libICE
        xorg.libSM
        xorg.libX11
        xorg.libXext
      ];

      unpackPhase = ''
        unzip $src
      '';

      installPhase = ''
        mkdir -p $out
        cp -r VitalInstaller/lib $out/lib
      '';
      postFixup = ''
        for file in \
          $out/lib/clap/Vital.clap \
          $out/lib/vst/Vital.so \
          $out/lib/vst3/Vital.vst3/Contents/x86_64-linux/Vital.so
        do
          patchelf --set-rpath "${lib.makeLibraryPath buildInputs}" $file
        done
      '';
    };

  vsts = with pkgs; [
    wineWowPackages.stagingFull
    pkgs.winetricks
    pkgs.yabridge
    pkgs.yabridgectl
    pkgs.zynaddsubfx # (is zyn-fusion what i actually want?)
    pkgs.lsp-plugins
    pkgs.calf
    pkgs.zam-plugins
    pkgs.eq10q
    pkgs.guitarix
    pkgs.gxplugins-lv2
    pkgs.CHOWTapeModel
    vital
  ];

  myRenoise = with pkgs;
    renoise.overrideAttrs (prev: {
      src = requireFile {
        name = "rns_343_linux_x86_64.tar.gz";
        sha256 = "10443pfm92lkf20p060pwy6ry46jzxk4wda5b0xvb0g07czi0kkh";
        message = "point me to your download";
      };
    });

in {
  imports = [ ./sway.nix ./waybar.nix ];

  home.packages = with pkgs;
    [
      d-spy
      clang_15
      surge
      surge-XT
      myRenoise
      helix
      asciinema
      asciinema-agg
      ytfzf
      multimarkdown
      libresprite
      pixelorama
      rx
      bitwarden
      blender
      borgbackup
      brightnessctl
      cmus
      coq
      gnome-disk-utility
      discord
      easyeffects
      libva-utils
      elvish
      firefox-wayland
      foliate
      gajim
      gimp
      cheese
      google-chrome # only really use it for GBF
      imv
      libnotify # for notify-send
      lxappearance
      (pkgs.callPackage ./mdloader.nix { })
      mpv
      neovim
      obsidian
      pavucontrol
      powertop
      prismlauncher
      pulseaudio # *only* for pactl
      tdesktop
      thunderbird
      usbutils
      vlc
      wally-cli # ergodox firmware flasher
      weechat
      nodejs
      python310
      toilet
      xdg-utils
      yarn
      gdb
      gnumake
      jq
      nixfmt-classic
      nmap
      perl
      tiled
      fusee-launcher
      cookiecutter

      cargo-nextest
      cargo-outdated
      cargo-flamegraph
      cargo-llvm-cov
    ] ++ vsts;

  home.sessionVariables = let
    makePluginPath = format:
      (lib.makeSearchPath format [
        "$HOME/.nix-profile/lib"
        "/run/current-system/sw/lib"
        "/etc/profiles/per-user/$USER/lib"
      ]) + ":$HOME/.${format}";
  in {
    DSSI_PATH = makePluginPath "dssi";
    LADSPA_PATH = makePluginPath "ladspa";
    LV2_PATH = makePluginPath "lv2";
    LXVST_PATH = makePluginPath "lxvst";
    VST_PATH = makePluginPath "vst";
    VST3_PATH = makePluginPath "vst3";
  };

  gtk.enable = true;
  gtk.theme = {
    name = "Adwaita-dark";
    package = pkgs.gnome-themes-extra;
  };

  qt = {
    enable = true;
    platformTheme.name = "adwaita";
    style.name = "adwaita-dark";
  };

  home.pointerCursor = {
    package = pkgs.capitaine-cursors-themed;
    gtk.enable = true;
    name = "Capitaine Cursors";
    x11.enable = true;
  };

  programs.beets = {
    enable = true;
    settings = {
      directory = "~/music";
      plugins = "fromfilename";
    };
  };

  programs.rofi = {
    enable = true;
    theme = ./interstellar.rasi;
  };

  programs.kitty = {
    enable = true;
    settings = {
      background = "#13131a";
      foreground = "#d0d4d8";

      font_family = "Source Code Pro";
      font_size = "11.0";
      undercurl_style = "thin-dense";
    };
  };

  services.mako = {
    enable = true;
    defaultTimeout = 6000;
    backgroundColor = "#13131AF8";
    borderColor = "#382D31";
    width = 350;
    height = 150;
    actions = false;
    extraConfig = ''
      [mode=do-not-disturb]
      invisible=1
    '';
  };

  services.kanshi = {
    enable = true;
    profiles = let
      resize_scratchpad =
        "${pkgs.sway}/bin/swaymsg '[app_id=scratchpad] resize set 960px 810px, move position center'";

    in {
      undocked.outputs = [{
        criteria = "eDP-1";
        status = "enable";
      }];
      docked.outputs = [
        {
          criteria = "GIGA-BYTE TECHNOLOGY CO., LTD. M28U 22100B011169";
          status = "enable";
          mode = "3840x2160";
          scale = 2.0;
        }
        {
          criteria = "eDP-1";
          status = "disable";
        }
      ];
      undocked.exec = resize_scratchpad;
      docked.exec = resize_scratchpad;
    };
  };
}
