{ stdenv, fetchFromGitHub, pkgs, ... }:

stdenv.mkDerivation rec {
  version = "1.0.7";
  name = "mdloader-${version}";
  src = fetchFromGitHub {
    owner = "Massdrop";
    repo = "mdloader";
    rev = version;
    sha256 = "sha256-ydi9XMHztmechVEfXD5gT0Solk3D2CGAp69xvXK5iYk=";
  };
  installPhase = ''
    mkdir -p $out/bin
    cp build/mdloader $out/bin
  '';
}
