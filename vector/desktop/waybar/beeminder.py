from datetime import datetime, timedelta
import json
from pathlib import Path
import requests
import sys
from typing import TypedDict

# How many hours before derailment should this be marked as unsafe?
# Used for habits like going to sleep aren't really useful to display
# as beemergencies earlier. If not specified, assumes Beeminder's
# usual 24-hour derailment.
#
# The key is habit IDs so I don't leak habit names into my public
# config. :p
_THRESHOLDS = {
    "627488903b53bf35fe663dfd": timedelta(hours=2),
    "627489023b53bf35fe663e01": timedelta(hours=4),
    "606a24293b53bf461c001311": timedelta(hours=8),
    "627838fe3b53bf365f66462a": timedelta(hours=4),
    "63f28fbaf0168a46d26f2345": timedelta(hours=4),
}
_URL = "https://www.beeminder.com/api/v1/users/hyperstrike/goals.json"
_ICON = "󰛡"  # hexagon-y icon


class _Goal(TypedDict):
    id: str
    slug: str
    losedate: int


def _goals(token: str) -> list[_Goal]:
    return requests.get(_URL, params={"auth_token": token}).json()


def _is_unsafe(goal: _Goal) -> bool:
    deadline = datetime.fromtimestamp(goal["losedate"])
    time_left = deadline - datetime.now()
    threshold = _THRESHOLDS.get(goal["id"], timedelta(hours=24))
    return time_left < threshold


def _output(goals: list[_Goal]) -> dict[str, str]:
    unsafe = [goal["slug"] for goal in _goals(token)
              if _is_unsafe(goal)]
    if unsafe:
        return {
            "class": "beemergency",
            "text": _ICON,
            "tooltip": "\n".join(unsafe)
        }
    else:
        return {
            "class": "ok",
            "text": _ICON,
            "tooltip": "good job :)"
        }


if __name__ == "__main__":
    token = Path(sys.argv[1]).read_text()
    goals = _goals(token)
    print(json.dumps(_output(goals)))
