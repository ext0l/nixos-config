{ config, pkgs, lib, ... }:

let
  beeminder = pkgs.writers.writePython3 "beeminder-waybar" {
    libraries = [ pkgs.python3Packages.requests ];
  } (builtins.readFile ./waybar/beeminder.py);

in {
  programs.waybar = {
    enable = true;
    systemd.enable = true;
    settings = [
      {
        layer = "top";
        height = 30;
        modules-left = [ "sway/workspaces" ];
        modules-center = [ "custom/cmus" ];
        modules-right =
          [ "custom/backups" "custom/beeminder" "custom/todoist" "clock" ];
        modules = {
          "sway/workspaces" = { format = "{name}"; };
          clock = {
            interval = 10;
            format = "{:%a %m/%d %H:%M}";
          };
          "custom/cmus" = {
            interval = 1;
            exec-if = "pgrep -x cmus";
            # `cut` is here because of GY!BE
            exec = ''cmus-remote -C "format_print '%a - %t'" | cut -c-60'';
            # annoying bug; can close when
            # https://github.com/Alexays/Waybar/issues/1757 lands in a branch
            escape = false;
          };
          "custom/backups" = {
            interval = 60;
            exec = "${./waybar/check-backups.sh} 2>/dev/null";
          };
          "custom/todoist" = {
            interval = 60;
            "format" = "{}";
            exec = "${./waybar/todoist.sh} /run/agenix/todoist-token";
            on-click = "xdg-open https://todoist.com/app";
          };
          "custom/beeminder" = {
            interval = 60;
            return-type = "json";
            exec = "${beeminder} /run/agenix/beeminder-token";
          };
        };
      }
      {
        layer = "top";
        position = "bottom";
        height = 30;
        modules-right = [ "cpu" "battery" "network" "pulseaudio" ];
        modules = {
          cpu = {
            interval = 1;
            format = "{usage:3}%";
          };
          network = {
            interface = "wlan0";
            interval = 3;
            format-wifi = "{signalStrength:3}%";
            format-disconnected = "󰖪";
            tooltip-format = "{ipaddr} @ {essid} {frequency}GHz";
          };
          battery = {
            format = "{icon}{capacity:3}%";
            format-charging = "󰂄{capacity:3}%";
            format-icons = [ "󰁻" "󰁽" "󰁿" "󰂁" "󰁹" ];
            states = { critical = 10; };
          };
        };
        pulseaudio = {
          format = "{icon}{volume:3}%";
          format-muted = " ---";
          format-icons = { default = [ "󰕿" "󰖀" "󰕾" ]; };
        };
      }

    ];
    style = builtins.readFile ./waybar/config.css;
  };
}
