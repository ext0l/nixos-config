{ config, pkgs, lib, fetchFromSavannah, ... }:

with lib;

{
  imports = [ ./nvim ];

  options.vector.fullConfigRepo = mkOption {
    type = types.bool;
    default = false;
    description = ''
      Whether this machine will have the entire configuration repo cloned.
      If true, some files are made out-of-store symlinks for faster iteration.'';
  };

  config = {
    home.sessionVariables.EDITOR = "hx";

    home.packages = with pkgs; [
      bat
      direnv
      fd
      just
      htop
      ripgrep
      starship
      tmux
      unzip
      cachix
    ];

    programs.direnv = {
      enable = true;
      nix-direnv.enable = true;
    };

    programs.fzf = {
      enable = true;
      enableFishIntegration = false;
    };

    programs.fish = {
      enable = true;
      shellInit = ''
        starship init fish | source
        # we need to rebind because fzf's nixpkgs integration *also* binds
        # things like C-r
        fzf_configure_bindings
      '';
      plugins = [{
        name = "fzf";
        src = pkgs.fetchFromGitHub {
          owner = "PatrickF1";
          repo = "fzf.fish";
          rev = "9caeb82c8f9ea83134ec70cf5a710b46de87d3e7";
          sha256 = "sha256-BTcZ8qOCU0iHlLgGLdfWWHKYsrRa7WO6EGuJy7ycpHo=";
        };
      }];
    };

    programs.git = {
      enable = true;
      userEmail = "ext0l@catgirl.ai";
      userName = "Ash";
      lfs.enable = true;
      aliases = {
        lol = "log --graph --decorate --pretty=oneline --abbrev-commit";
        lola = "lol --all";
      };
      ignores = [ "*~" "\\#*\\#" ];
      extraConfig = {
        init.defaultBranch = "main";
        branch.autosetupmerge = "always";
      };
    };

    programs.zoxide.enable = true;

    programs.tmux = {
      enable = true;
      baseIndex = 1;
      keyMode = "vi";
      newSession = true;
      sensibleOnTop = true;
      prefix = "C-a";
      extraConfig = builtins.readFile ./tmux.conf;
      plugins = with pkgs; [ tmuxPlugins.pain-control tmuxPlugins.sessionist ];
    };

    home.stateVersion = "18.09";
  };
}
