# Neovim configuration. I set up the nvim configuration itself using symlinks
# for rapid iteration; this module just contains auxiliary programs it needs.

{ config, pkgs, lib, ... }:

{
  home.packages = with pkgs; [
    neovim # duh
    lua-language-server # for editing the lua config
  ];
}
