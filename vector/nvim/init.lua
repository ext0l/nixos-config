-- throat-clearing setup {{{
-- do this as early as possible to set up leader correctly
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- set up lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable",             -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)
-- }}}

vim.diagnostic.config({ update_in_insert = true })
require("lazy").setup({
  'tpope/vim-fugitive',
  {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate"
  },
  {
    "folke/which-key.nvim",
    event = "VeryLazy",
    init = function()
      vim.o.timeout = true
      vim.o.timeoutlen = 300
    end,
    opts = {}
  },
  "neovim/nvim-lspconfig",
  "hrsh7th/nvim-cmp",
  { "folke/neodev.nvim", opts = {} },
  {
    "folke/tokyonight.nvim",
    lazy = false,
    priority = 1000,
    opts = {},
  } }, {})

vim.cmd [[colorscheme tokyonight]]

require 'nvim-treesitter.configs'.setup({
  highlight = {
    enable = true,
    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    additional_vim_regex_highlighting = false,
  },
})

require 'lspconfig'.lua_ls.setup {}

-- vim: foldmethod=marker tabstop=2 softtabstop=2 expandtab
