# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports = [ # Include the results of the hardware scan.
    ../common.nix
    ../desktop.nix
    ./hardware-configuration.nix
  ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.kernelPackages = pkgs.linuxPackages_latest;

  # enable hardware video decoding
  nixpkgs.config.packageOverrides = pkgs: {
    vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
  };
  hardware.opengl = {
    enable = true;
    extraPackages = with pkgs; [
      intel-media-driver # LIBVA_DRIVER_NAME=iHD
      vaapiIntel # LIBVA_DRIVER_NAME=i965 (older but works better for Firefox/Chromium)
      vaapiVdpau
      libvdpau-va-gl
    ];
  };

  musnix.enable = true;

  services.getty.autologinUser = "vector";

  networking.hostName = "communio";
  networking.wireless.iwd.enable = true;
  networking.networkmanager.enable = true;
  networking.networkmanager.wifi.backend = "iwd";
  hardware.system76.enableAll = true;
  hardware.enableRedistributableFirmware = true;

  hardware.bluetooth = {
    enable = true;
    input = { General = { UserspaceHID = true; }; };
  };

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.upower = {
    enable = true;
    criticalPowerAction = "Hibernate";
  };

  services.avahi = {
    enable = true;
    nssmdns4 = true;
  };

  services.printing.enable = true;
  services.printing.drivers = [ pkgs.hplip ];

  environment.systemPackages = [ pkgs.tailscale pkgs.cosmos ];
  services.tailscale.enable = true;
  networking.firewall.checkReversePath = "loose"; # for tailscale

  home-manager.users.vector.vector.fullConfigRepo = true;

  programs.adb.enable = true;
  users.users.vector.extraGroups = [ "adbusers" "networkmanager" ];

  services.automatic-timezoned.enable = true;

  services.geoclue2.enable = true;

  services.cosmos = {
    enable = true;
    config = { geolocate = "ip-api"; };
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.05"; # Did you read the comment?
}

