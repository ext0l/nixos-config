{ config, pkgs, ... }:

{
  imports = [ ../cachix.nix ];

  documentation.man = {
    enable = true;
    generateCaches = true;
  };

  nix = {
    settings.auto-optimise-store = true;
    # for nix-direnv, flakes
    extraOptions = ''
      experimental-features = nix-command flakes
      keep-outputs = true
      keep-derivations = true
    '';
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 14d";
    };
  };

  nixpkgs.config.allowUnfree = true;
  # annoyingly necessary for obsidian
  nixpkgs.config.permittedInsecurePackages = [ "electron-25.9.0" ];

  networking = { useDHCP = false; };

  # Set up me.
  users.users.vector = {
    shell = pkgs.fish;
    isNormalUser = true;
    home = "/home/vector";
    description = "just this robot girl";
    extraGroups = [ "wheel" "networkmanager" "audio" ];
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDeCD+KbuTRwY2ImukD13KKARB2XeIYpob5mOZ2cb3xJjD5EsRdixBkbhrNLNyTvGs+OVPM7IiBNHh5qthcEGqUxp10JUAfhQQTEYQL/yMGoAhckbLpIcnn8Ss38iNV7KvXLvOwCVKuC7vsz8hmi50dNoZy8zI7QUXpLgmw2BrbzbtXRceArurYoeItYPaQXmljwLRauh32Jj56owwk5VCFcs64pyryd0KZF8EnPiQAaOboGCl/kwpkfrqMALzdefCeSmjiQQM3v0vMB0BjnZFWW7migO/J5G5zqRUl3qEjFktFLF8o+vp4ctI4FT2IHqeQypANLfVsgX2CGmIGfXJr vector@superluminal-steel"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHUqhRMMaD7ji3zcwPwhWfzx+1QRW1cXfcG1expueUfI vector@communio"
    ];
  };

  services.xserver.xkb = {
    layout = "us";
    variant = "colemak_dh_ortho";
  };
  console.useXkbConfig = true;

  programs.fish.enable = true;

  users.users.root.openssh.authorizedKeys.keys =
    config.users.users.vector.openssh.authorizedKeys.keys;

  home-manager.useGlobalPkgs = true;
  home-manager.users.vector = import ../vector;
}
