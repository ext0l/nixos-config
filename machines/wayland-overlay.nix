{ lib, pkgs, repos, ... }:

{
  nixpkgs.overlays = [
    (self: super: {
      wlopm = pkgs.stdenv.mkDerivation rec {
        pname = "wlopm";
        version = repos.wlopm.rev;
        src = repos.wlopm.outPath;

        installFlags = [ "PREFIX=${placeholder "out"}" ];
        buildInputs = with pkgs; [ wayland wayland-protocols ];

        meta = with lib; {
          description = "Wayland output power management";
          license = licenses.gpl3Only;
          platforms = platforms.linux;
        };
      };
    })
  ];
}
