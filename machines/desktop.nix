# Common configuration for 'desktop'-type machines, meaning anything I
# can sit down at. Right now this is just superluminal-steel.

{ config, pkgs, lib, ... }:

{
  imports = [ ./wayland-overlay.nix ];
  virtualisation.docker.enable = true;

  fonts = {
    packages = with pkgs; [
      unifont
      siji
      meslo-lg
      tamzen
      uw-ttyp0
      terminus_font
      ipafont
      kochi-substitute
      source-code-pro
      nerd-fonts.sauce-code-pro
      nerd-fonts.meslo-lg
    ];
    enableDefaultPackages = true;
    fontconfig.defaultFonts = { monospace = [ "Source Code Pro" ]; };
  };

  services.flatpak.enable = true;

  xdg.portal = {
    enable = true;
    xdgOpenUsePortal = true;
    config.common.default = "*"; # eh
    extraPortals = with pkgs; [
      xdg-desktop-portal-wlr
      xdg-desktop-portal-kde
      xdg-desktop-portal-gtk
    ];
    wlr = {
      enable = true;
      settings = {
        screencast = {
          output_name = "eDP-1";
          max_fps = 30;
        };
      };
    };
  };

  programs.steam = {
    enable = true;
    remotePlay.openFirewall =
      true; # Open ports in the firewall for Steam Remote Play
    dedicatedServer.openFirewall =
      true; # Open ports in the firewall for Source Dedicated Server
  };

  # pipewire is the Cool New Sound Thing
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    jack.enable = true;
    pulse.enable = true;
  };

  services.blueman.enable = true;

  services.udev.extraRules = ''
    ${builtins.readFile ./50-qmk.rules}

    SUBSYSTEMS=="usb", ATTRS{manufacturer}=="NVIDIA Corp.", ATTRS{product}=="APX", TAG+="uaccess"
  '';
  users.users.vector = { extraGroups = [ "dialout" ]; };

  services.logind.extraConfig = ''
    HandlePowerKey=ignore
    HandleSuspendKey=ignore
  '';

  age.secrets = {
    "rsync.borg".file = ./rsync.borg.age;
    "rsync.borg".owner = "vector";
    "shattered-maelstrom.borg".file = ./shattered-maelstrom.borg.age;
    "shattered-maelstrom.borg".owner = "vector";
  };

  services.borgbackup.jobs = let
    makeJob = { repo, passfile, extraArgs ? "" }: rec {
      inherit repo;
      inherit extraArgs;

      paths = "/home/vector";
      encryption = {
        mode = "repokey";
        passCommand = "cat ${passfile}";
      };
      environment.BORG_RELOCATED_REPO_ACCESS_IS_OK = "yes";
      compression = "auto,zstd";
      # persistentTimer = true;
      startAt = "*-*-* 04:00:00";
      exclude = map (x: "sh:" + paths + "/" + x) [
        ".cache"
        ".cargo"
        "code/*/target"
        "code/others/*/target"
        "**/node_modules"
        "**/.tox"
        ".local/share/Steam/steamapps/shadercache"
        ".local/share/Steam/steamapps/common"
      ];
      user = "vector";
      group = "users";
      extraCreateArgs = "--stats --checkpoint-interval 600";
      prune.keep = {
        within = "3d";
        daily = 7;
        weekly = 4;
        monthly = 6;
        yearly = 1;
      };
    };
  in {
    shattered-maelstrom = makeJob {
      repo = "ssh://root@shattered-maelstrom/srv/borg/against-all-authority";
      passfile = config.age.secrets."shattered-maelstrom.borg".path;
    };
    rsync-net = makeJob {
      repo =
        "ssh://fm2206@fm2206.rsync.net/data2/home/fm2206/backups/superluminal-steel";
      passfile = config.age.secrets."rsync.borg".path;
      extraArgs = "--remote-path=borg1";
    };
  };

  # GNOME-y things.

  services.gnome.gnome-keyring.enable = true;
  programs.dconf.enable = true;

  security.pam.services.swaylock = { };

  age.secrets = {
    beeminder-token.file = ./beeminder-token.age;
    beeminder-token.owner = "vector";
    todoist-token.file = ./todoist-token.age;
    todoist-token.owner = "vector";
  };

  services.syncthing = {
    enable = true;
    user = "vector";
    dataDir = "/home/vector/.syncthing";
  };
  # for syncthing
  networking.firewall.allowedTCPPorts = [ 8384 22000 ];
  networking.firewall.allowedUDPPorts = [ 22000 21027 ];

  home-manager.users.vector = import ../vector/desktop;
}
