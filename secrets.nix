let
  superluminal-steel = [
    # Keys to my desktops. Includes the root key for switching and the
    # non-root key for editing.
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEezzLaAr3H7I6Dug9T2gMiPBsqOM9locF8VtjtGBZvg root@superluminal-steel"
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDeCD+KbuTRwY2ImukD13KKARB2XeIYpob5mOZ2cb3xJjD5EsRdixBkbhrNLNyTvGs+OVPM7IiBNHh5qthcEGqUxp10JUAfhQQTEYQL/yMGoAhckbLpIcnn8Ss38iNV7KvXLvOwCVKuC7vsz8hmi50dNoZy8zI7QUXpLgmw2BrbzbtXRceArurYoeItYPaQXmljwLRauh32Jj56owwk5VCFcs64pyryd0KZF8EnPiQAaOboGCl/kwpkfrqMALzdefCeSmjiQQM3v0vMB0BjnZFWW7migO/J5G5zqRUl3qEjFktFLF8o+vp4ctI4FT2IHqeQypANLfVsgX2CGmIGfXJr brightflame@superluminal-steel"
  ];

  communio = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHUqhRMMaD7ji3zcwPwhWfzx+1QRW1cXfcG1expueUfI vector@communio"
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILfFgHKhCz7uo/l4rbZbA88O6OYPtL12lnzxZY8FfNUW root@against-all-authority"
  ];

  aleph = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFuD89amnXPuvIGPEwPb1vDqacBwV4OFdrNUaKGy41Dq vector@aleph"
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIOJVbHYAYZZbYcMgupiarqNQS8e2t6gXZWbUsiepPk+ root@aleph"
  ];

  makeFiles = keys: files:
    builtins.listToAttrs (map (file: {
      name = "${file}.age";
      value.publicKeys = keys;
    }) files);

in makeFiles (superluminal-steel ++ communio ++ aleph) [
  "machines/beeminder-token"
  "machines/todoist-token"
  "machines/rsync.borg"
  "machines/shattered-maelstrom.borg"
]
