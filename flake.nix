{
  inputs = {
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    cosmos.url = "git+https://codeberg.org/ext0l/cosmos";
    cosmos.inputs.nixpkgs.follows = "nixpkgs";
    nixpkgs.url = "nixpkgs/nixos-unstable";
    agenix.url = "github:ryantm/agenix";
    agenix.inputs.nixpkgs.follows = "nixpkgs";
    wlopm = {
      url = "git+https://git.sr.ht/~leon_plickat/wlopm";
      flake = false;
    };
    musnix.url = "github:musnix/musnix";
    musnix.inputs.nixpkgs.follows = "nixpkgs";
    lix-module = {
      url =
        "https://git.lix.systems/lix-project/nixos-module/archive/2.91.1-2.tar.gz";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    fw-fanctrl = {
      url = "github:TamtamHero/fw-fanctrl/packaging/nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, home-manager, agenix, wlopm, musnix, cosmos
    , lix-module, fw-fanctrl }: {
      nixosConfigurations.communio = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          home-manager.nixosModules.home-manager
          ({ pkgs, ... }: {
            nix = { registry = { nixpkgs.flake = nixpkgs; }; };
            system.configurationRevision =
              nixpkgs.lib.mkIf (self ? rev) self.rev;
            nixpkgs.overlays = [
              cosmos.overlays.default
              (import ./sway-spy.nix)
            ];
          })
          ./machines/communio/configuration.nix
          agenix.nixosModules.age
          musnix.nixosModules.musnix
          cosmos.nixosModules.default
        ];
        specialArgs.repos = { inherit wlopm; };
      };
      nixosConfigurations.aleph = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          home-manager.nixosModules.home-manager
          ({ pkgs, ... }: {
            nix = { registry = { nixpkgs.flake = nixpkgs; }; };
            system.configurationRevision =
              nixpkgs.lib.mkIf (self ? rev) self.rev;
            nixpkgs.overlays = [
              (import ./flake-compat.nix)
              cosmos.overlays.default
              (import ./sway-spy.nix)
            ];
          })
          ./machines/aleph/configuration.nix
          lix-module.nixosModules.default
          agenix.nixosModules.age
          musnix.nixosModules.musnix
          cosmos.nixosModules.default
          fw-fanctrl.nixosModules.default
        ];
        specialArgs.repos = { inherit wlopm; };
      };
    };
}
