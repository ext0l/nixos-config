final: prev: {
  swayfx-unwrapped = prev.swayfx-unwrapped.overrideAttrs {
    patches = prev.swayfx-unwrapped.patches ++ [ ./vector/desktop/spy.patch ];
  };
}
